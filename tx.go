package stx

import (
	"context"
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"log"
)

const MYSQL_DEADLOCK_FOUND_WHEN_TRYING_TO_GET_LOCK = 1213

type Transaction interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	Prepare(query string) (*sql.Stmt, error)
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

type TransactionalContext interface {
	context.Context
	Transaction
}

func NewTransactionalContext(ctx context.Context, transaction *sql.Tx) TransactionalContext {

	type internalTransactionalContext struct {
		context.Context
		Transaction
	}

	return &internalTransactionalContext{
		Context:     ctx,
		Transaction: transaction,
	}
}

//type TxError error

type TxClosure func(tx TransactionalContext) (txe error)

func newTransaction(ctx context.Context, conn *sql.DB, txOptions *sql.TxOptions, txFunc TxClosure) (err error) {
	tx, err := conn.BeginTx(ctx, txOptions)
	if err != nil {
		return
	}
	defer func() {
		if p := recover(); p != nil {
			_ = tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			_ = tx.Rollback() // err is non-nil; don't change it
		} else {
			err = tx.Commit() // err is nil; if Commit returns error update err
		}
	}()
	// wrap *sql.Tx in context and call the closure
	return txFunc(NewTransactionalContext(ctx, tx))
}

func WithTransaction(ctx context.Context, conn *sql.DB, txOptions *sql.TxOptions, txFunc TxClosure) (err error) {
	if txJoin, ok := ctx.(TransactionalContext); ok {
		return txFunc(txJoin)
	}
	//TODO make retry count a parameter,
	// and implement exponential backoff policy
	retryCount := 1
	for {
		err = newTransaction(ctx, conn, txOptions, txFunc)
		if err == nil {
			return
		}
		if retryCount > 0 {
			//deadlock is the only retry condition
			mysqlErr, ok := err.(*mysql.MySQLError)
			if ok && mysqlErr.Number == MYSQL_DEADLOCK_FOUND_WHEN_TRYING_TO_GET_LOCK {
				log.Printf("Dealock detected, will retry: %v", mysqlErr)
				retryCount -= 1
				err = newTransaction(ctx, conn, txOptions, txFunc)
			}
		}
		return
	}
}

package stx

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"testing"
)

func ApiFunc2(ctx context.Context, a int, b string) (result string, err error) {

	txFunc := func(transaction TransactionalContext) error {
		result = fmt.Sprintf("%d:%s", a, b)
		return nil
	}
	if err = WithTransaction(ctx, conn, nil, txFunc); err != nil {
		return
	}
	return result, nil
}

func ApiFunc1(ctx context.Context, x int, y string, z float64) (result int, err error) {
	result = -1
	txFunc := func(tx TransactionalContext) (err error) {
		if x+len(y)+int(z) != 0 {
			var s string
			s, err = ApiFunc2(tx, x+1, y+"test")
			_ = s
			if err != nil {
				return
			}
			result = len(s)
			return errors.New("bingo")
		}
		return nil
	}
	err = WithTransaction(ctx, conn, &sql.TxOptions{Isolation: sql.LevelSerializable}, txFunc)
	if err != nil {
		return
	}
	return result, nil
}

var conn *sql.DB // Global but of course for the prototype sake

func TestOneFunc(t *testing.T) {
	ctx := context.Background()
	s, err := ApiFunc2(ctx, 42, "life")
	if err != nil {
		t.Errorf("expected error, got %v", err)
	}
	if s != "42:life" {
		t.Errorf("expected 42:life, got %s", s)
	}

}

func TestFuncChain(t *testing.T) {
	ctx := context.Background()
	i, err := ApiFunc1(ctx, 0, "0", 0)
	if err == nil {
		t.Errorf("expected error, got %v", err)
	}
	if i != 7 {
		t.Errorf("expected 7, got %d", i)
	}
	//fmt.Printf("result: value %d, error %v\n", i, err)
}

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	//defer tearDown()
	defer func() { os.Exit(m.Run()) }()
	err := setup()
	if err != nil {
		fmt.Printf("setup failer, error: %v", err)
	}
}

func setup() (err error) {
	conn, err = sql.Open("mysql", "bgvault:secret@tcp(127.0.0.1:33060)/bgvault")
	return
}

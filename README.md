# Simple Transaction #

Transaction support for API whose methods, for simplicity, can call other public methods of the same API
Perhaps not a best idea but makes things simple, especially when you already have a similar implementation.

The called public API method with automatically join transaction started by the first method. 

### Go module bitbucket.org/teosoft123/stx ###

* Quick summary
* Current Version: v0.1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
